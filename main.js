function detectPalindrome(str) {
    let arrOfStrChars = str.split('');
    let reverseStr = arrOfStrChars.reverse().join('');
    return str === reverseStr;
}
